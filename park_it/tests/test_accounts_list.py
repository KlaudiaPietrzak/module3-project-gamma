from fastapi.testclient import TestClient
from main import app
from queries.accounts import AccountRepo

client = TestClient(app)  # The Swagger-swap ;)


class AccountRepoMock:
    def get_all_accounts(self):
        return []


def test_get_accounts():
    # Arrange
    app.dependency_overrides[AccountRepo] = AccountRepoMock
    # Act
    response = client.get("/api/accounts")
    # Assert
    assert response.status_code == 200
