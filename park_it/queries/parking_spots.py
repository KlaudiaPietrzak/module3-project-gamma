from pydantic import BaseModel
from queries.pool import pool
from datetime import datetime


class ParkingSpotIn(BaseModel):
    number: int
    street_name: str
    street_abbr: str
    city: str
    state: str
    zipcode: int
    date: datetime
    available: bool
    owner_id: int


class ParkingSpotOut(BaseModel):
    id: int
    number: int
    street_name: str
    street_abbr: str
    city: str
    state: str
    zipcode: int
    date: datetime
    lat: float
    lon: float
    available: bool
    owner_id: int


class ParkingSpotsOut(BaseModel):
    parking_spots: list[ParkingSpotOut]


class ParkingSpotRepo:
    def create(self, parking_spot: ParkingSpotIn, lat: int, lon: int) -> ParkingSpotOut:  # noqa E501
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO parking_spot (
                        number,
                        street_name,
                        street_abbr,
                        city,
                        state,
                        zipcode,
                        date,
                        lat,
                        lon,
                        available,
                        owner_id
                        )
                    VALUES (
                        %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
                    )
                    RETURNING id;
                    """,
                    [
                        parking_spot.number,
                        parking_spot.street_name,
                        parking_spot.street_abbr,
                        parking_spot.city,
                        parking_spot.state,
                        parking_spot.zipcode,
                        parking_spot.date,
                        lat,
                        lon,
                        parking_spot.available,
                        parking_spot.owner_id,
                    ],
                )
                id = result.fetchone()[0]
                return ParkingSpotOut(
                    id=id,
                    number=parking_spot.number,
                    street_name=parking_spot.street_name,
                    street_abbr=parking_spot.street_abbr,
                    city=parking_spot.city,
                    state=parking_spot.state,
                    zipcode=parking_spot.zipcode,
                    date=parking_spot.date,
                    lat=lat,
                    lon=lon,
                    available=parking_spot.available,
                    owner_id=parking_spot.owner_id,
                )

    def get_all_parking_spots(self):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id, number, street_name, street_abbr, city, state, zipcode, date, lat, lon, available, owner_id
                    FROM parking_spot
                    ORDER BY date
                    """  # noqa E501
                )
                results = []
                for row in result.fetchall():
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def get_one_parking_spot(self, id: int) -> ParkingSpotOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id, number, street_name, street_abbr, city,state, zipcode, date, lat, lon, available, owner_id
                    FROM parking_spot
                    WHERE id = %s;
                    """,  # noqa E501
                    [id],
                )
                record = result.fetchone()
                if record is None:
                    return None
                return ParkingSpotOut(
                    id=record[0],
                    number=record[1],
                    street_name=record[2],
                    street_abbr=record[3],
                    city=record[4],
                    state=record[5],
                    zipcode=record[6],
                    date=record[7],
                    lat=record[8],
                    lon=record[9],
                    available=record[10],
                    owner_id=record[11],
                )

    def update_parking_spot(
        self, parking_spot_id, parking_spot, lat: int, lon: int
    ) -> ParkingSpotOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE parking_spot
                    SET number = %s
                        , street_name = %s
                        , street_abbr = %s
                        , city = %s
                        , state = %s
                        , zipcode = %s
                        , date = %s
                        , lat = %s
                        , lon = %s
                        , available = %s
                        , owner_id = %s
                    WHERE id = %s
                    RETURNING id, number, street_name, street_abbr, city, state, zipcode, date, lat, lon, available, owner_id
                    """,  # noqa E501
                    [
                        parking_spot.number,
                        parking_spot.street_name,
                        parking_spot.street_abbr,
                        parking_spot.city,
                        parking_spot.state,
                        parking_spot.zipcode,
                        parking_spot.date,
                        lat,
                        lon,
                        parking_spot.available,
                        parking_spot.owner_id,
                        parking_spot_id,
                    ],
                )
                record = None

                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                return record

    def delete_parking_spot(self, parking_spot_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM parking_spot
                    where id = %s
                    """,
                    [parking_spot_id],
                )
