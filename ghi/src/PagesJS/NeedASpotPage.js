import React, { useState, useEffect, useRef } from "react";
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';
// eslint-disable-next-line import/no-webpack-loader-syntax
import mapboxgl from '!mapbox-gl';
import "../styles/NeedASpotPage.css"
import { useAuthContext } from "../Authentication"


mapboxgl.accessToken = 'pk.eyJ1Ijoia2F0ZWx5bm1hcnRpbiIsImEiOiJjbGJsZzUxY3kwNzdwM3ZwbWU5MHVscWFtIn0.LpdNKJEO0ig4ELfCy8pBKA'

export default function Map() {
    const { token } = useAuthContext();
    const mapContainer = useRef(null);
    const map = useRef(null);
    const [lng, setLng] = useState(-70.9);
    const [lat, setLat] = useState(42.35);
    const [zoom, setZoom] = useState(9);
    const [parkingSpots, setParkingSpots] = useState([]);
    const [mapMarkers, setMapMarkers] = useState([]);
    const [userInputMarkers, setUserInputMarkers] = useState([]);



    async function getDatabaseParking() {
        for (const userInputMarker of userInputMarkers) {
            userInputMarker.remove();
        }
        const url = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/parking_spots/`;
        const response = await fetch(url, { headers: { Authorization: `Bearer ${token}` } });
        if (response.ok) {
            const data = await response.json();
            const updatedParking = data.parking_spots.filter(parking_spot => {
                const isAvailable = parking_spot.available === true
                const isLatInMap = Math.abs(parking_spot.lat) >= (Math.abs(lat) - 0.2) && parking_spot.lat <= (Math.abs(lat) + 0.2)
                const isLngInMap = Math.abs(parking_spot.lon) >= (Math.abs(lng) - 0.2) && parking_spot.lon <= (Math.abs(lng) + 0.2)
                const newDate = parking_spot.date.toLocaleString()
                return isAvailable && isLatInMap && isLngInMap && newDate
            });
            const userMarkers = [];
            for (let spot of updatedParking) {

                const marker = new mapboxgl.Marker({
                    color: "#EE964B",
                    draggable: true
                }).setLngLat([spot.lon, spot.lat])
                    .setPopup(new mapboxgl.Popup().setText(`${spot.number + " " + spot.street_name.replaceAll("%20", " ") + " " + spot.street_abbr}`))
                    .addTo(map.current);
                userMarkers.push(marker);
            }
            setParkingSpots(updatedParking);
            setUserInputMarkers(userMarkers);
        }
    }

    async function getApiParking() {
        for (const mapMarker of mapMarkers) {
            mapMarker.remove();
        }
        const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/parking.json?type=poi&proximity=${lng},${lat}&limit=10&access_token=${mapboxgl.accessToken}`
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const markers = [];
            for (let spot of data.features) {
                const marker = new mapboxgl.Marker({
                    color: "#0D3B66",
                    draggable: true
                }).setLngLat([spot.center[0], spot.center[1]])
                    .setPopup(new mapboxgl.Popup().setText(`${spot.place_name}`))
                    .addTo(map.current);
                markers.push(marker)
            }
            setMapMarkers(markers);
        }
    }

    async function updateParking(parking_spot_id, e) {

        const currentParkingSpot = parkingSpots.filter(parkingSpot => parkingSpot.id === parking_spot_id)

        const data = {
            number: currentParkingSpot[0].number,
            street_name: currentParkingSpot[0].street_name,
            street_abbr: currentParkingSpot[0].street_abbr,
            city: currentParkingSpot[0].city,
            state: currentParkingSpot[0].state,
            zipcode: currentParkingSpot[0].zipcode,
            date: currentParkingSpot[0].date,
            available: false,
            owner_id: currentParkingSpot[0].owner_id,
        }

        const url = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/parking_spots/${parking_spot_id}`
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`,
            }
        }
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            getDatabaseParking();
        }
    }



    useEffect(() => {
        if (map.current) return; // initialize map only once
        map.current = new mapboxgl.Map({
            container: mapContainer.current,
            style: 'mapbox://styles/mapbox/streets-v12?optimize=true',
            center: [lng, lat],
            zoom: zoom
        });
        map.current.addControl(
            new MapboxGeocoder({
                accessToken: mapboxgl.accessToken,
                mapboxgl: mapboxgl
            })
        );
        map.current.addControl(new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true,
            showUserHeading: true
        }));

        map.current.addControl(new mapboxgl.NavigationControl());


    }, [lat, lng, zoom]);


    useEffect(() => {
        if (!map.current) return;
        map.current.on('move', () => {
            setLng(map.current.getCenter().lng.toFixed(4));
            setLat(map.current.getCenter().lat.toFixed(4));
            setZoom(map.current.getZoom().toFixed(2));
        });

    });


    return (

        <div >
            <section className="hero_map">
                <div ref={mapContainer} className="container" style={{ position: "relative" }}>
                    <button id="show-parking" className="btn_mini_parking" onClick={() => {
                        getApiParking()
                        getDatabaseParking()
                    }
                    }>Show Parking Near Me</button>
                </div>
                <div className={`container_map ${(parkingSpots.length === 0) ? "hidden" : ""}`}>
                    <h2 className="title-map">Available Parking Spots:</h2>
                    {parkingSpots.map(parkingSpot =>
                        <div className="parking_spot" key={parkingSpot.id}>
                            <div>
                                <span className="address_city">{parkingSpot.city.replaceAll("%20", " ")}</span>
                                <span className="address">{parkingSpot.number} {parkingSpot.street_name.replaceAll("%20", " ")} {parkingSpot.street_abbr}</span>
                            </div>
                            <div className="btn-group">
                                <button onClick={e => updateParking(parkingSpot.id, e)} className="btn_mini">Taken!</button>
                            </div>
                        </div>
                    )}
                </div>
            </section>
        </div>

    );

}
