import { useState } from "react";
import { useToken } from "../Authentication";
import { useNavigate, Link } from "react-router-dom";


const accountData = {
    "username": "",
    "password": "",
}

export default function LoginComponent() {
    const { token, login } = useToken();
    const [loginData, setLoginData] = useState(accountData);
    let navigate = useNavigate()

    function handleInput(e) {
        setLoginData(prevState => {
            return { ...prevState, [e.target.id]: e.target.value }
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        await login(loginData.username, loginData.password);
        setLoginData(accountData);
        navigate("/")
    };

    if (token) {
        navigate("/");
    }
    return (
        <>


            {/* sign in page--------------- */}
            <div className="container">
                <form action="/" className="form" id="form2" onSubmit={(e) => handleSubmit(e)}>
                    <h2 className="form__title">Log In</h2>
                    <input
                        id="username"
                        type="text"
                        autoComplete="off"
                        placeholder="Email"
                        onChange={(e) => handleInput(e)}
                        className="input"
                        required
                    />
                    <input
                        id="password"
                        type="password"
                        placeholder="Password"
                        onChange={(e) => handleInput(e)}
                        className="input"
                    />
                    <button className="btn" type="submit">
                        Log In
                    </button>
                    <Link className="form__link" to="/signup">Don't have an account? Sign up here!</Link>
                </form>
            </div>

        </>
    );
}
