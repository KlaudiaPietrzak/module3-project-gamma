11/15 - worked with Katelyn  on getting some back-end endpoints identified; created an amalgamated login/signup page concept with Katelyn and Janete

11/16 - experementation with maps to display pushpins, pushpins w/ radius, attach links to pushpins, change

11/17 - morning standup. pre-lunch group activtity: working with authentication/JWT tokens setup on back-end.

11/20 - continued work on matching infoboxes to lists in the mapview.

11/21 - Group discussion on mapping out flow of pages:
        login/out on the same vs. 2 dif pages; composition.
      - Added paignated list of 10 neariest parking entities as pushpins.

11/22 - Fixing bugs in the the pushpins/list display >-<
      - Added  new Geoding Dataflow template *just in case* we need an alternate way to pull loc.data

11/28 - A humbling morning; we have decided to go with Mapbox, for the more explicit documentation, over bingmaps. The switch to mapbox has
        already yielded significantly better results!

11/29 - ALWAYS ADD YOUR MODULES TO DEPENDANCIES. First half of the day went to diagnosing a
        container-crashing error brought about by an unused module: "Iconify".
      - Studied Mapbox documention for Raster display, Dataset importation via Mapbox's Geocoding API,

11/30 - Kaitlyn and I spent all of today working within mapbox's development service. We had success in adding the geocoder module to enable a search box feature and the ability to center the map  on the user location. Another feature added was a navigation control. One of the hangups was getting the font of the auto-filled suggestions in the searchbox to be a more reasonable size.


12/1 - Today, Kaitlyn and I made huge strides in our goal to the get the map funcioning. We are now able to get the 10 nearest API-supplied parking entities to our user's location, and we are able to visibly display user-supplied parking spots(THE WHOLE PREMISE OF OUR APP). We also installed pop-up bubbles that display the address of the spot for that bubble. Jenny visited us just atfer lunch to bounce sum bug-busting ideas off of us, and it turns out the bug in her async function that she needing help fixing paved the way for our being able to discover the proper async functionality required to load our parkling spots outside of the map! Big progress, lots more to come!


12/2 - Worked on setting up functioning sidebar for map object on NeedASpot.js; Janete gave me a whole heap of help getting the CSScontainers for our map and user post list to look nice(They aren't looking THAT nice, but with Janete's help they look way better than if I had done them on my own) Also: trying to get the data from out Userspot DB is proving to be quite the hurdle!


12/4 - Minor change to App.js -> set link "targets ='_self'" so that clicking on links will stop opening those links in new tab

12/5 - Working with the NeedAspot page today; trying to cleanup the CSS so that the map an user provided spots list will show up aesthetically. Still working on trying to get user populated spots list in the table on the NeedASpot Page.

12/6 - Worked(unsuccesfully) on using the AuthToken to offer a potected view of the NavBar that would show only the Home our Icon Home button and the Login button to users who are not logged in, and an account button that is the logged in Users name if a user is logged in. Katelyn fixed it and made it really pretty!(My teammates are definitely heavyweight programmers O=('.')Q ; I'm still trying to get to 'golden gloves' status haha).

12/7 - Spent the morning trying to resolve a 'module not found error' logging as a result of improperly trying to import our Logo to the homepage. Got the logo to appear, and started marrying the NavBar across all of our pages; working on getting the "Logout" button to stop being the black-sheep of the gorup. It's stubborn. 2pm: NAvBar is one "login/signup" route away from working *EXACTLY* how we want it to!  ** 10pm oil = css styling for button + a few quick blurbs about the team/writing in the Design specs/attaching a link to the team wireframe in ./docs/README.MD
