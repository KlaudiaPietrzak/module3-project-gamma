import { BrowserRouter, Routes, Route } from "react-router-dom";
import NeedASpotPage from "./PagesJS/NeedASpotPage";
import "./App.css";
import HomePage from "./PagesJS/HomePage";
import Nav from "./Nav";
import "./HomePage.css";
import ParkingSpotForm from "./PagesJS/ParkingSpotForm.js";
import AccountPage from "./PagesJS/AccountPage.js";
import ParkingSpotEditForm from "./PagesJS/ParkingSpotEditForm.js";
import { AuthProvider, useToken } from "./Authentication";
import LoginComponent from "./accounts/LoginComponent.js";
import SignupComponent from "./accounts/SignupComponent.js";
import PrivateRoutes from './utils/PrivateRoute'


function GetToken() {
  useToken();
  return null
}

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');
  return (
    <>
      <BrowserRouter basename={basename}>
        <Nav />
        <AuthProvider>
          <GetToken />
          <div>
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/login" element={<LoginComponent />} />
              <Route path="/signup" element={<SignupComponent />} />
              <Route element={<PrivateRoutes />}>
                <Route path="/parking-spot-form" element={<ParkingSpotForm />} />
                <Route path="/account" element={<AccountPage />} />
                <Route path='/parking-spot/:id' element={<ParkingSpotEditForm />} />
                <Route path="/parking-spots" element={<NeedASpotPage />} />
              </Route>
            </Routes>
          </div>
        </AuthProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
