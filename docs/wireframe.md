# Wireframe

## Homepage (_none authenticated_)

This is the first page a user will interact with, it has only one button. Once user clicks that one button it will send them to the signin/signup page accordingly.

![unauthorized](/docs/images/unauthorizedwireframepage.jpg)

## Signup

Signup page includes inputs for name, email, and password, users are able to also toggle between logging in or signing up for a new account.

![signup](/docs/images/signupwireframepage.jpg)

## Login

Login page includes inputs for username and password, username being the email you signed up with. Users are able to toggle between logging in and signing up on this page as well.

![login](/docs/images/loginwireframepage.jpg)

## Directory (_shown after login_)

This directory is shown at the top right of all pages after authentication has been detected. The "home button" has been replaced with our logo but has a link to get redirected back to our home page shown below.

![dir](/docs/images/directorywireframepage.jpg)

## Homepage (_after authentication_)

The homepage buttons change and the directory is displayed after authentication has been successful. Any onclick on sign in or sign up page get redirected to this homepage after the request has been sent successfully.

![authorized](/docs/images/authorizedwireframepage.jpg)

## New Parking spot Form (_form_)

This form is accessed through the home page in both ways, it can be accessed through the found a spot button below our logo or in the directory. Once on click it becomes a blank form with info needed to add a spot to your account so that other users are able to see it.

![parkingspot](/docs/images/parkingspotwireframepage.jpg)

## Account page (_shows account details and posted spots_)

The account page can be accessed through the homescreen by clicking the account button in the nav directory in the upper right corner of the home page after being authenticated successfully.

![account](/docs/images/accountwireframepage.jpg)

## Parking Spot Edit Form (_form_)

This form can be accessed through the account page after creating a new parking spot from your account. This form is used to edit an existing parking spot. It comes with inputs such as street, city, state, and zipcode.

![parkingspotedit](/docs/images/parkingspoteditwireframepage.jpg)

## Need a spot (_map and list view of spots_)

This page contains our main attraction which is our map and list of spots available near you. This page can be accessed two ways, by clicking the button under our logo labeled "find a spot" or in our directory. Once navigated to the page you will have a map and the option to click on either your location or search for a location. Once you are at your location then it lists close parking spots near that area by pinpoint and list. Also updates according to area it is being hovered over.

![map](/docs/images/mapwireframepage.jpg)
