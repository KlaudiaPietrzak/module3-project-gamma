import nameplate from "./../assets/images/parkitnameplate.png";


function HomePage() {
  const userIsLoggedIn = localStorage.getItem("userId")

  return (
    <div>
      <section >
        <header className="hero-header">
          <h1>
            <img src={nameplate} className="hero-title" alt=" " />
          </h1>
        </header>
        <div className="hero-footer">
          <a id="signin" className={`btn_mini_sign_in button ${userIsLoggedIn ? "hidden" : ""}`} href="/park-it/login">
            Log In to find a spot!
          </a>
          <a id="foundaspot" className={`btn_mini_sign_in button ${userIsLoggedIn ? "" : "hidden"}`} href="/park-it/parking-spot-form">
            Found a Spot?
          </a>
          <a id="needaspot" className={`btn_mini_sign_in button ${userIsLoggedIn ? "" : "hidden"}`} href="/park-it/parking-spots">
            Need a Spot?
          </a>
        </div>
      </section>
    </div>
  );
}
export default HomePage;
