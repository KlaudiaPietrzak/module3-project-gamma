fastapi[all]>=0.78.0
uvicorn[standard]==0.17.6
psycopg[binary,pool]
jwtdown-fastapi>=0.2.0
pytest==7.1.2
