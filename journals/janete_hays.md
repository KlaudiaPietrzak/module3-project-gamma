## November 16, 2022

Today, I worked on:

More front end content, Finished up our mock up of our sign in and sign out page. Was more focused on
converting the HTML that I wrote into React. Had some bugs in regards to running the React Server,
will continue to work on that tomorrow. At the moment I am working on finishing up our mock up of our
front page and navigation bar.

##November 17, 2022
Started on the signin/signout page had some issues with the javascript functionality of the transitions.
Figured out that I needed to import the script file into our app.js. Started writing components of the
other pages that needed to be built for our website. Pulled updates for the YAML file from my partners.

##November 18, 2022
Continued on the signin/signout page and started looking for templates for our form pages that we will have
decided to add routes for future pages that would be incorporated in our project. Was having issues with the
Nav bar showing up somewhere I didn't want it to show up. Ended up removing NAV.js function contents since
I embedded our nav into each of our pages not as a header.

##November 21, 2022

Today, I worked on:

making sure the Javascript portion of the signin/signup page was working. Starting on the React Frontend
portion of the Home page which is our most important page. Created a Nav bar and added our logo to our
front page that is also imbedded with a link to take us back to the home page if elsewhere. Decided to use
some SCSS or SASS for front end other then CSS. Had some trouble getting it to work but finally got
it up and running. Removed the "night mode" of the application because it was unnecessary with the little
wording we have on our front page. Pushed changes and pulled any changes that I was missing, Had some issues
with accidently forking the project and not working on the main project. Had to figure out how to get back
to our main project, managed to find a way and pushed everything to the correct branches and project.

##November 22, 2022

Today, I worked on:

My own branch on the actual project, I began to work on authentication to get the backend to connect to the
frontend of our project. It has been a work in progress, have watched some videos and attempted to use our
learn but it still isn't clicking.

##November 23, 2022

Today, I worked on:

Authentication still, have noticed a pattern that they usually build authentication under two components
other then one. Keep trying to research a way to build the authentication plateform into just one component
. More research is being done here including watching tons of youtube videos and trial and error.

##November 24, 2022

Today, I worked on:

More Authentication, I finally got something to work last night but unfortunetly the Javascript gave out
and the animations for the slide effect no longer work. I have to figure out an alternative and move the
Javascript around so that we can see it. Have tried adding it into the react portion with a script tag
and it still wouldn't read it.

##November 25, 2022

Today, I worked on:

Trying to see if I can split up the current template we have into two componenets and have them talk to
eachother. I have created some new components known as loginpagecomponent and signuppagecomponent and split
the code into two. I can see the css but the Javascript is still not being read and I keep getting an error
about useEffect hook not being able to be in the top of the page.

##November 28, 2022

##November 29, 2022

Today, I worked on:

the authorization for our signin/signup page, currently having issues with implementing our model to our
front end React. Have tried a bunch of try and catches and it isn't working, will be looking for an alternative
tomorrow by either making another page that reflects what a signed in user sees and having a successful
sign in take them to that said page. Also assisted Klaudia on one of our forms to adjust an input field
that was not stretching to a certain length.

##December 1, 2022

Today, I worked on:

More front end work, tackled some more authentication in the front end, managed to be able to login and
redirect the webpage over to home page. A challange I am facing at the moment is being able to use existing
information that has been inputted in the signup form rather then a master login. Also having issues with
having the button's change when someone is logged in rather then logged out. Will be meeting up with Riley
tomorrow to talk over possible help.

##December 5, 2022

Today, I worked on:

Trying to get the Javascript to work on two components rather then having it on one for the signin/signup
pages. The authentication seems to only work on two componenets so the theme of our page would have to
be split. No matter where I put the Javascript for some reason it is not read by either components. May
have to look for an alternative tomorrow if I can't get this to work today. I took a break after researching
how to link up to components and worked on some homepage button's. Was having issues with the backend
and wondering how to hide certain buttons for authorized and unauthorized personel. Got some help from
Katelyn which assisted me in figuring out what was missing in the code to get it working.

##December 6, 2022

Today, I worked on:

Finding a new login/logout template for our pages, unfortunetly I was unable to get the Javascript to work
on the awesome slidein/slidout signin/signup feature. I found a simple yet sophesticated signin/signout
template that resembles a frosted glass container. I managed to be able to code in the React portion and get
it showing up with the css that was implemented. I also managed to have everything link together, which
means the authentication portion of the app was speaking to the rest of the front end components.

##December 7, 2022

Today, I worked on:

Trying to pull and merge changes that have been happening lately. After the first merge/pull my site
broke completely. I have been trying to revert back to previous changes as well as work on the login/signin
page. Finally was able to revert back to previous and commit but when I pulled again the css was not
showing up on my end again... Have decided to fully restart my computer and close down all my containers,
images, and volumes and rebuild them. I went ahead and attempted to pull again, my signin/signout features
finally work AGAIN! I will call it a night and test them out tomorrow.

##December 8, 2022

Today, I worked on:

Trying to get my website to work again, last night I left with everything working just fine, Today I come back
to a website that no longer works AGAIN. Have decided to assist Katelyn, one of our group members on her version
since hers is up and running. We went ahead and changed the whole theme of the signin/signout page from
what we had in mind before. We attempted to keep the asthetic of the page flowing... We have decided to
clean up our folders and such and make sure everything is working nicely in her version so that we can pull
when complete.

##December 9, 2022

Today, I worked on:

Unit Tests, we finally made it to the end of the project and we are working on unit testing. I watched some videos and decided on creating a test for the token.
