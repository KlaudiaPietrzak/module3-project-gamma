from fastapi.testclient import TestClient
from main import app
from routers.accounts import AccountToken

client = TestClient(app)


class AccountTokenMock:
    def get_token(self):
        return " "


def test_token() -> None:
    app.dependency_overrides[AccountToken] = AccountTokenMock
    response = client.get("/token")
    assert response.status_code == 200
