import requests
import os

MAP_BOX_API_KEY = os.environ["MAP_BOX_API_KEY"]


def get_point(number, street_name, street_abbr, city, state, zipcode):
    res = requests.get(
        f"https://api.mapbox.com/geocoding/v5/mapbox.places/{number}%20{street_name}%20{street_abbr}%2C%20{city}%2C%20{state}%20{zipcode}.json?proximity=ip&types=place%2Cpostcode%2Caddress&access_token={MAP_BOX_API_KEY}"  # noqa E501
    )
    coordinates = res.json()["features"][0]["center"]

    return {"lon": coordinates[0], "lat": coordinates[1]}
