from fastapi import (
    Depends,
    Response,
    APIRouter,
)

from authenticator import authenticator

from queries.parking_spots import (
    ParkingSpotIn,
    ParkingSpotOut,
    ParkingSpotRepo,
    ParkingSpotsOut,
)
from acls import get_point

router = APIRouter()


@router.post("/api/parking_spots", response_model=ParkingSpotOut)
def create_parking_spot(
    parking_spot: ParkingSpotIn,
    repo: ParkingSpotRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    street_name = parking_spot.street_name.replace(" ", "%20")
    coordinates = get_point(
        parking_spot.number,
        street_name,
        parking_spot.street_abbr,
        parking_spot.city,
        parking_spot.state,
        parking_spot.zipcode,
    )
    lat = coordinates["lat"]
    lon = coordinates["lon"]

    parking_spot = repo.create(parking_spot, lat, lon)

    return parking_spot


@router.get("/api/parking_spots", response_model=ParkingSpotsOut)
def parking_spot_list(repo: ParkingSpotRepo = Depends()):
    return {
        "parking_spots": repo.get_all_parking_spots(),
    }


@router.get("/api/parking_spots/{parking_spot_id}", response_model=ParkingSpotOut)  # noqa E501
def get_parking_spot(
    parking_spot_id: int,
    response: Response,
    repo: ParkingSpotRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    record = repo.get_one_parking_spot(parking_spot_id)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.put("/api/parking_spots/{parking_spot_id}", response_model=ParkingSpotOut)  # noqa E501
def update_parking_spot(
    parking_spot_id: int,
    parking_spot: ParkingSpotIn,
    response: Response,
    repo: ParkingSpotRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    coordinates = get_point(
        parking_spot.number,
        parking_spot.street_name,
        parking_spot.street_abbr,
        parking_spot.city,
        parking_spot.state,
        parking_spot.zipcode,
    )
    lat = coordinates["lat"]
    lon = coordinates["lon"]

    record = repo.update_parking_spot(parking_spot_id, parking_spot, lat, lon)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.delete("/api/parking_spots/{parking_spot_id}", response_model=bool)
def delete_parking_spot(
    parking_spot_id: int,
    repo: ParkingSpotRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    repo.delete_parking_spot(parking_spot_id)
    return True
