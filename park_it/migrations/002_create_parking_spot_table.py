steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE parking_spot (
            id SERIAL PRIMARY KEY NOT NULL,
            number INTEGER NOT NULL,
            street_name VARCHAR(100) NOT NULL,
            street_abbr VARCHAR(15) NOT NULL,
            city VARCHAR(20) NOT NULL,
            state VARCHAR(2) NOT NULL,
            zipcode INTEGER NOT NULL,
            lat FLOAT8 NOT NULL,
            lon FLOAT8 NOT NULL,
            date TIMESTAMP NOT NULL,
            available BOOLEAN,
            owner_id INTEGER NOT NULL REFERENCES accounts("id") ON DELETE CASCADE
        );
        """  # noqa E501

        """
        ALTER TABLE parking_spot
        ALTER COLUMN available
        SET DEFAULT TRUE;
        """,

        # "Down" SQL statement
        """
        DROP TABLE parking_spot;
        """,

    ],


]
