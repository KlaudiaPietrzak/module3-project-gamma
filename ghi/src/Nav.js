import { NavLink } from 'react-router-dom';
import React from 'react';
import { useToken } from './Authentication';
import logo from './assets/images/plogonobackground.png';
import './HomePage.css';

function Nav() {
    const { logout } = useToken();

    const handleLogout = async (e) => {
        e.preventDefault();
        await logout();
    };

    const userIsLoggedIn = localStorage.getItem("userId")


    return (
        <nav className="navbar-menu">
            <div>
                <ul>
                    <li className="li_logo">
                        <NavLink to="/">
                            <img src={logo} id="logo" alt=" " />
                        </NavLink>
                    </li>
                    <li>
                        <NavLink style={userIsLoggedIn ? {} : { display: 'none' }} to="/parking-spots">Parking Spots</NavLink>
                    </li>
                    <li>
                        <NavLink style={userIsLoggedIn ? {} : { display: 'none' }} to="/parking-spot-form">Found A Spot?</NavLink>
                    </li>
                    <li>
                        <NavLink style={userIsLoggedIn ? {} : { display: 'none' }} to="/account">Account</NavLink>
                    </li>
                    <li>
                        <NavLink className="nav-logout" style={userIsLoggedIn ? {} : { display: 'none' }} to="/" onClick={handleLogout}>Logout</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default Nav;
