import React, { useEffect, useState } from "react";
import "../styles/AccountPage.css";
import { useAuthContext } from "../Authentication";



const AccountPage = () => {
  const [accounts, setAccounts] = useState([]);
  const [parkingSpots, setParkingSpots] = useState([]);
  const { token } = useAuthContext();

  //Fetching account details
  useEffect(() => {
    async function getAccountInfo() {
      const url = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/accounts/${localStorage.getItem("userId")}`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setAccounts(data);
      }
    }
    getAccountInfo();
  }, [])

  //Fetching parking spots
  useEffect(() => {
    async function getParkingSpots() {
      const url = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/parking_spots`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();

        setParkingSpots(data.parking_spots.filter(parking_spot => parking_spot.owner_id === parseInt(localStorage.getItem("userId"))))
      }
    }
    getParkingSpots();
  }, [setParkingSpots])


  //Delete method
  const onDelete = async (parking_spot_id, e) => {
    e.preventDefault();
    const url = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/parking_spots/${parking_spot_id}`
    const fetchConfig = {
      method: "DELETE",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }
    await fetch(url, fetchConfig);
    const newParkingSpotList = parkingSpots.filter(parkingSpot => parkingSpot.id !== parking_spot_id)
    setParkingSpots(newParkingSpotList)
  }


  return (
    <>
      <section className="hero">
        <div className="container_account_page">
          <h2 className="title">Your account information:</h2>
          <div className="input-grid">
            <div>
              <img src="https://as1.ftcdn.net/v2/jpg/00/57/04/58/1000_F_57045887_HHJml6DJVxNBMqMeDqVJ0ZQDnotp5rGD.jpg" alt="" className="profile-img" />
            </div>
            <div className="title">
              <div>
                <div key={accounts.id}>
                  <h3>Name:</h3>
                  <p>{accounts.name}</p>
                  <h3>Email:</h3>
                  <p>{accounts.username}</p>
                </div>
              </div>
            </div>
            <div className="title">
              <h3>You have added</h3>
              <p>{parkingSpots.length}</p>
              <h3>spots</h3>
            </div>
          </div>
        </div>
        <div className={`container_account ${(parkingSpots.length === 0) ? "hidden" : ""}`}>
          <h2 className="title">Your parking spots:</h2>
          {parkingSpots.map(parkingSpot =>
            <div className="parking_spot" key={parkingSpot.id}>
              <div>
                <span className="address_city">{parkingSpot.city.replaceAll("%20", " ")}</span>
                <span className="address">{parkingSpot.number} {parkingSpot.street_name.replaceAll("%20", " ")} {parkingSpot.street_abbr}</span>
              </div>
              <div className="btn-group">
                <a href={`/park-it/parking-spot/${parkingSpot.id}`} target="_self"><button className="btn_mini">Edit</button></a>
                <button onClick={e => onDelete(parkingSpot.id, e)} className="btn_mini">Delete</button>
              </div>
            </div>
          )}
        </div>
      </section>

    </>
  );
};
export default AccountPage;
