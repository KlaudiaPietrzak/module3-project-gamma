import json  # noqa F401
from fastapi.testclient import TestClient
from main import app
from queries.parking_spots import ParkingSpotRepo
from authenticator import authenticator

client = TestClient(app)  # replacing swagger in code


def get_current_account_data_mock():
    return {"id": 31, "username": "G@gmail.com"}


class ParkingSpotRepoMock:
    def get_one_parking_spot(self, parking_spot_id):
        response = {
            "id": parking_spot_id,
            "number": 1234,
            "street_name": "Sesame",
            "street_abbr": "St.",
            "city": "Memphis",
            "state": "TN",
            "zipcode": 38138,
            "date": "2022-12-07T15:52:21.671000",
            "lat": 36.14227,
            "lon": -90.880565,
            "available": True,
            "owner_id": 31,
        }
        return response

    def get_all_parking_spots(self):
        return []


# Klaudia
def test_get_all_parking_spots():
    # Arrange
    app.dependency_overrides[ParkingSpotRepo] = ParkingSpotRepoMock
    # Act
    response = client.get("/api/parking_spots")
    # Assert
    assert response.status_code == 200
    assert response.json() == {"parking_spots": []}
    # clean up
    app.dependency_overrides = {}


# Katelyn
def test_get_one_parking_spot():
    # Arrange
    app.dependency_overrides[ParkingSpotRepo] = ParkingSpotRepoMock
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_account_data_mock
    # Act
    response = client.get("/api/parking_spots/31")
    # Assert
    # 1. get a 200 if function works
    assert response.status_code == 200
    # 2. Should call repo.get_one_parking_spot(parking_spot_id)
    assert response.json()["number"] == 1234
    assert response.json()["zipcode"] == 38138
    assert response.json()["available"]

    # clean up
    app.dependency_overrides = {}
