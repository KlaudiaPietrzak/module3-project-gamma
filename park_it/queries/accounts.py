from pydantic import BaseModel
from queries.pool import pool


class DuplicateAccountError(ValueError):
    pass


class Account(BaseModel):
    id: int
    name: str
    username: str  # email
    hashed_password: str


class AccountIn(BaseModel):
    name: str
    username: str  # email
    password: str


class AccountOut(BaseModel):
    id: int
    name: str
    username: str  # email
    # password: str


class AccountsOut(BaseModel):
    accounts: list[AccountOut]


class AccountRepo:
    def create(self, info: AccountIn, hashed_password: str) -> Account:
        # connect to the database
        with pool.connection() as conn:
            # get a cursor
            with conn.cursor() as db:
                # run our INSERT statement
                result = db.execute(
                    """
                    INSERT INTO accounts
                        (name, username, hashed_password)
                    VALUES
                        (%s, %s, %s)
                    RETURNING id;
                    """,
                    [info.name, info.username, hashed_password],
                )
                id = result.fetchone()[0]
                return Account(
                    id=id,
                    name=info.name,
                    username=info.username,
                    hashed_password=hashed_password,
                )

    def get(self, username: str) -> Account:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id, name, username, hashed_password
                    FROM accounts
                    where username = %s;
                    """,
                    [username],
                )
                record = result.fetchone()
                if record is None:
                    return None
                return Account(
                    id=record[0],
                    name=record[1],
                    username=record[2],
                    hashed_password=record[3],
                )

    def get_all_accounts(self):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id, name, username
                    FROM accounts
                    ORDER BY name
                    """
                )

                results = []
                for row in result.fetchall():
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                    results.append(record)
                return results

    def get_one_account(self, id: int) -> AccountOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id, name, username
                    FROM accounts
                    WHERE id = %s;
                    """,
                    [id],
                )
                record = result.fetchone()
                print(record)
                if record is None:
                    return None
                return AccountOut(
                    id=record[0],
                    name=record[1],
                    username=record[2],
                )
