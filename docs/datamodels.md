# Data Models

### Accounts

| Name            | Type | Not Null | Primary Key |
| --------------- | ---- | -------- | ----------- |
| id              | Int  | True     | Yes         |
| Name            | Str  | Yes      | No          |
| username        | Str  | True     | No          |
| hashed_password | Str  | True     | No          |

### Parking Spot

| Name        | Type      | Not Null | Primary Key |
| ----------- | --------- | -------- | ----------- |
| id          | Int       | True     | Yes         |
| Number      | Int       | True     | No          |
| Street Name | Str       | True     | No          |
| Street Abbr | Str       | True     | No          |
| City        | Str       | True     | No          |
| State       | Str       | True     | No          |
| Zipcode     | Int       | True     | No          |
| Lat         | Float     | True     | No          |
| Lon         | Float     | True     | No          |
| Date        | Timestamp | True     | No          |
| Available   | Boolean   | False    | No          |
| Owner Id    | Int       | True     | No          |
