12/12/2022
Today is the final day of the exam. I focused on writing the readme file.

12/09/2022
Today our team focused on qriting the tests. I created a test to get list of all parking spots.

12/08/2022
Final changes for the app, deleted redundant code, moving css files into seperate folder, final css touches

12/07/2022
Today we came across merging conflict so we spent most of the day resolving that issue with my team.

12/06/2022
Today I worked with my teamate to set up edit method for parking spot edit form.

12/05/2022
Today I worked on DELETE parking spot method. We came across some errors like 500 and 422 but it was resolved with help of one of the instructors

12/02/2022
Today I focused on displaying parking spots on account page

12/01/2022
Today I worked on displaying account info on account page and on connecting backend and frontend for parking spot form

11/30/2022
Connecting backend and frontend for parking spot form

11/29/2022
Today I worked on fornted for account page which contained both account info and list of posted parking spots by specific user

11/28/2022
Worked on front end for parking spot form/parking spot edit form

11/23/2022
Worked on front end for parking spot form/parking spot edit form

11/22/2022
Worked on front end for parking spot form/parking spot edit form

11/21/2022
Worked on front end for parking spot form/parking spot edit form

11/17/2022
Worked on Authentication/Authorization

11/16/2022
I worked on endpoints. We have written all endpoints code in required documentation in the applciation

11/15/2022
App idea presentation to the group. in the afternoon we worked on endpoints for our app and Bing Map API

11/14/2022
Because of last minute idea change, we had to adjust our diagrams and templates in Excalidraw. We had a brainstorming session to decide on the final details of how the app is going to work. Together we have decided on the color scheme for the logo. Janete created a logo on Canva.com

11/10/2022
We have consulted our idea with one of the instructors and together we have decided that the e-commerce site is going to be beyond our time frame and capabilities. We had to come up with a new app idea or transform our toy trading app. We have decided to go with a new idea, an app called ParkIt (crowd-sourced parking spot finder app).

11/9/2022
We have decided that we are done with wireframing so we focused on discussing models and views for the app. Started browsing the internet for free map API

11/8/2022
Continued brainstorming session. We focused on wireframing the app and creating more detailed templates for our app, for example, the home page, login and logout page, and donation page.

11/7/2022
First day of Gamma project. We found out who is going to be in the group. First brainstorming session. We ended up with the idea to create a toy trading app. We started creating basic diagrams in Excalidraw.
