Week 1: project brainstorming. Started with toy trading app ideas, however, we realized setting up an ecommerce site was a bit beyond our capabilities/time frame for this project. We have started with a new idea: ParkIt (crowd-sourced parking spot finder app). Ended the week with finding our API, Bing Maps API, that we will use to populate a list of local public parking areas based on user location.
Week 2:
## 11/14/22
Today, I worked on:
* Restructuring wireframe and logo creation

We worked together as a group to finalize our wireframe design for our ParkIt idea. We also collaborated together to create our porducts logo.

## 11/15/22
Today, I worked on:
* API endpoint design:

I worked with Kaludia and Andrew to determine our API endpoint design for the login, signup, main page, parking spot submission form, and partially through our parking list view/map. We are having a hard time to fully visualize how our backend code will be at this moment with our calls to third-party APIs.

We also presented our idea to the group today with good feedback!

## 11/16/22
Today, I worked on:
* posgres database set up, API endpoint design, docker yaml file updates

I set up our project database using postgresql. I also updated our yaml file to support this updates throughout the project. I worked with Klaudia to finalize API endpoint design. We do realize that these endpoints can change as the project evolves. I uploaded the wireframe and created the api.md in docs. I am currently debating between setting up a script to set up our database tables vs using migrations.

## 11/17/22
Today, I worked on:
* progres database set up, authentication, log in, log out.

I ended up reconfiguring our postgres set up to use migrations instead of a sql file. I learned about using the F2 key today from the video on authentication in order to select all items on a page. Worked for a long time with Klaudia to get authentication set up. Had some issues when it came to our Account Repo.

## 11/18/22
Today, I worked on:
* Authentication. Third party API manipulation.

The lecture this morning was very enlightening. I was able to get our authetication, log in, log out working from the lecture Dalonte gave! We are now working on deciding how to save our user input addresses in the back end in order to populate them on our dynamic map on the front end.

## 11/21/22
Today, I worked on:
* parking spot database table

I worked on creating our data table to save user input parking spot addresses. I also wrote an acls file to access the BING third party api to convert that address to lat and lon data. This will be used to display these user input addresses onto our front end map. HOPEFULLY!

## 11/22/22
Today, I worked on:
* finalizing endpoints as well as dynamic map manipulation.

The database table for our user input addresses is finally complete. I ran into an issue for saving the lat and lon numbers. It turns out my model for ParkingSpotOut specified that lat and lon should be type "int" when it should have been "float". Once I made that change the processes for creating and storing a parking space worked! Now I am working on the front end map display and queries to adjust the map based on user inputs/location.


## 11/28/22
Today, I worked on:
* reformatting code to use MapBox API instead of Bing Maps

Over the break, I had a chance to dive deeper into the dynamic map that we planned to use with through the Bing Maps API. We have been having trouble implementing this feature using the documentation available as well as research. We have no decided to change our API due to issues we have been having. We will be using MapBox. They have really nice documentation. I have already started the implementation of their dynamic map in our frontend. I will change the back end to pull from this API in order to store our users inputted addresses.

I have reformatted our backend database and API call to use MapBox instead of Bing Maps. I am setting up our views to have the authenticator requirement if we are requiring users to be logged in.

I also set up the rest of our endpoints for accounts and parking spots; getting one, getting all, and creating.

I was also able to get our map to show up on our front end, so now we will start manipulating it to show the info that we need.

## 11/29/22

Today, I worked on:

Today I have set up the boolean field on our table for the availability of our parking spots to default to true. I ran into some issues trying to figure out how to create a parking spot with my queries after altering the table to default to true. It took me a couple alterations, but I got it working this morning!

created the get token endpoint that I missed when I first set up the back end auth.

Set up the update endpoint for the parking spot and spent a while figuring out a bug "typeError: type is not JSON serializable. It took Riley looking at my code to figure out I inserted an "=" where there should have been a ":".


## 11/30/22

Today, I worked on:

Today, I have been working on getting the front end dynamic map functioning. We currently have the search, zoom, and use user location functions working. The next step is to populate parking areas based on the location of that map. Lastly we will need to add a data layer to display any user submitted parking spaces we have available in that area!


## 12/01/22

Today, I worked on:

Today, we have gotten our map functioning to display parking from the Mapbox as well as parking from our own database! It was a lot of work to figure out the best way to display the marker. Right now we are working through re-rendering data from MapBox because we can only display 10 data points at a time.


## 12/02/22
Today, I worked on:
front end auth

Today I have been working on getting our front end auth functioning. I had an issue where I was submitting "unprocessable entities" to our backend auth due to some differences in the names of properties. I ended up fixing those issues and was able to log in. However, now I am having an issue of not being authorized to sign up.

We are also running into bugs with our address input data because of spaces in the input. So I am editing the backend to insert special characters if there is more than one name in the city.

## 12/5/22
Today, I worked on:
front end auth, set up a new API "delete" route, and worked on final functionality of our dynamic map for listing parking spots.

Today I finalized the front-end login, signup, and logout. I also fixed an issue with some of our queries. If we have any streets with multiple names, there will be spaces in the queries to mapbox, so I fixed our router to insert the correct special characters into any spaces. I also helped klaudia set up the delete route for parking spaces, as well as sort out resetting our state in our parking spot submission forms. We have created a list of final MVP items in order to be ready for submission on Monday. It feels like a lot, but we will get it done! I am working on finalizing how our map page will function! We currently need to create a list that will show the details of our user submitted spots.

## 12/6/22

Today, I worked on:
dyanmic map, account page, and local storage.

Today I revamped our dynamic map because of issues we were having with our parking spot markers. I originally had the logic set up to reset the center(lng, lat) whenever the map moved, but that caused an issue with setting the state. As the map moved, it would rapidly load and set the longitude and latitude state as the map moved. This caused problems with the state due to other steps that removed markers as new ones were loaded. Long story short. I had to completely change the way the map functions. The map still moves to the user's location, allows for searching locations, and will display parking spots as well as our database parking. I also set up local storage to store the userID that is associated with the logged in user/token. Then I updated the code to display the user's acount based on who is logged in.


## 12/7/22

Account page (tally, logged in user, filter parking spots)
parking spot form (logic to add special characters)
edit parking form (logic to use params without infinite loop)
nav bar. Also wrote logic to show nav bar only when the user is logged in.
home page button logic, shows when logged in, hidden when user is logged out.

## 12/8/22

fixed logout/login issues.
fixed nav bar to align at top of page with logo left justified and the logout button to be a NavLink instead of a button.
fixed signup and login page css as well as their links.
update function to use "taken!" button on map page
updated to hide available spots unless there are some in the area.
set up login and signup navigation.

## 12/9/22

Wrote the unit test to test our get_one_parking_spot route.
Created private routes for our front end.
Worked on file organization.

## 12/10/22

deployment stuff again
