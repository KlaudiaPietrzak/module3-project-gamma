import { useState } from "react";
import { useToken } from "../Authentication";
import { useNavigate, Link } from "react-router-dom";

const accountData = {
    "name": "",
    "username": "",
    "password": "",
}

export default function SignupComponent() {
    const { token, signup } = useToken();
    const [signupData, setSignupData] = useState(accountData)
    let navigate = useNavigate()

    function handleInput(e) {
        setSignupData(prevState => {
            return { ...prevState, [e.target.id]: e.target.value }
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        await signup(signupData.name, signupData.username, signupData.password);
        setSignupData(accountData);
        navigate("/")
    };

    if (token) {
        navigate("/");
    }

    return (
        <>
            <div className="container">
                <div className="container">
                    <form action="/" className="form" id="form1" onSubmit={async (e) => await handleSubmit(e)}>
                        <h2 className="form__title">Sign Up</h2>
                        <input
                            id="name"
                            type="text"
                            placeholder="Name"
                            required
                            autoComplete="off"
                            onChange={(e) => handleInput(e)}
                            className="input"
                        />
                        <input
                            id="username"
                            type="email"
                            placeholder="Email"
                            required
                            autoComplete="off"
                            onChange={(e) => handleInput(e)}
                            className="input"
                        />
                        <input
                            id="password"
                            type="password"
                            placeholder="Password"
                            required
                            onChange={(e) => handleInput(e)}
                            className="input"
                        />
                        <button className="btn">Sign Up</button>
                        <Link className="form__link" to="/login">Have an account? Log in here!</Link>
                    </form>
                </div>
            </div>
        </>
    );
}
