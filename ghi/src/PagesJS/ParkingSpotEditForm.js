import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "../styles/ParkingSpot.css";
import { useAuthContext } from "../Authentication"

function Inputs(props) {
  const { value, onChange, type, id, placeholder } = props;

  return (
    <div className="form-input-wrapper flexbox-left">
      <input
        value={value}
        onChange={onChange}
        required
        type={type}
        id={id}
        placeholder={placeholder}
        className="form-input"
      />
    </div>
  )
}

const ParkingSpotEditForm = () => {
  const [street, setStreet] = useState('');
  const [cities, setCity] = useState('');
  const [states, setStates] = useState([]);
  const [zipcode, setZipcode] = useState('');
  // const [parkingSpot, setParkingSpot] = useState([]);
  const [stateAbbr, setStateAbbr] = useState('');
  const { token } = useAuthContext();
  let { id } = useParams()

  //Hook to get list of states
  //getting original data
  useEffect(() => {
    async function getStates() {
      const url = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/states`;
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setStates(data);
      }
    }
    getStates();

    async function getParkingSpot() {
      if (token) {
        const url = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/parking_spots/${id}`;
        const response = await fetch(url, { headers: { Authorization: `Bearer ${token}` } });
        if (response.ok) {
          const data = await response.json();
          // setParkingSpot(data)

          setStreet(data.number + " " + data.street_name + " " + data.street_abbr);
          setCity(data.city);
          setStateAbbr(data.state);
          setZipcode(data.zipcode);
        }
      }
    }
    getParkingSpot();
  }, [token, id]);


  //logic for handle submit

  const elements = (street.split(" "));

  const number = elements.shift();

  const street_abbr = elements.pop()

  const street_name_with_spaces = elements.join(" ")
  const street_name = street_name_with_spaces.replaceAll(" ", "%20")


  const city = cities.replaceAll(" ", "%20")


  const today = new Date();

  async function handleSubmit(e) {
    e.preventDefault();
    const data = {
      number: number,
      street_name: street_name,
      street_abbr: street_abbr,
      city: city,
      state: stateAbbr,
      zipcode: zipcode,
      date: today,
      available: true,
      owner_id: localStorage.getItem("userId"),
    };

    //Edit logic
    const url = `${process.env.REACT_APP_PARK_IT_API_HOST}/api/parking_spots/${id}`
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
    }
    await fetch(url, fetchConfig);

  };



  return (
    <>
      <section className="hero_parking">
        <div className="container">
          <form onSubmit={handleSubmit} className="form">
            <h2 className="form__title">Parking Spot Edit Form</h2>
            <div className="form-input-max">
              <Inputs
                value={street.replaceAll("%20", " ")}
                onChange={e => setStreet(e.target.value)}
                type="text"
                id="street"
                placeholder="Street"
              />
            </div>
            <div className="form-input-grid">
              <Inputs
                value={cities.replaceAll("%20", " ")}
                onChange={e => setCity(e.target.value)}
                type="text"
                id="city"
                placeholder="City"
              />
              <div className="flexbox-left">
                <select className="form-input" onChange={e => setStateAbbr(e.target.value)} value={stateAbbr}>
                  <option>State</option>
                  {states.map(state => <option key={state.abbreviation} value={state.abbreviation}>{state.name}</option>)}
                </select>
              </div>
              <Inputs
                value={zipcode}
                onChange={e => setZipcode(e.target.value)}
                type="text"
                id="zipcode"
                placeholder="Zipcode"
              />
            </div>
            <button className="btn">Save Changes</button>
          </form>
        </div>
      </section>
    </>
  );
};

export default ParkingSpotEditForm;
